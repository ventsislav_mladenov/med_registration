﻿using System;
using System.Collections.Generic;
using MedRegistration.Data;

namespace MedRegistration.Areas.Reports.Models
{
    public class ReservationLogModel
    {
        public int? UserId { get; set; }

        public int? DoctorId { get; set; }

        public int? PatientId { get; set; }

        public DateTime? FromTime { get; set; }

        public DateTime? ToTime { get; set; }

        public List<ReservationLog> Logs { get; set; } 
    }
}