﻿using System.Linq;
using System.Web.Mvc;
using System.Data.Entity;
using MedRegistration.Areas.Reports.Models;
using MedRegistration.Controllers;
using MedRegistration.Data;
using MedRegistration.Infrastructure.Authorization;

namespace MedRegistration.Areas.Reports.Controllers
{
    [ReportCheck]
    public class ReservationLogController : BaseController
    {
        [HttpGet]
        public ActionResult Index()
        {
            FillViewBag();
            return View(new ReservationLogModel());
        }

        [HttpPost]
        public ActionResult Index(ReservationLogModel model)
        {
            FillViewBag();
            using (var context = new DataContext(lazyLoading: false))
            {
                var query = from l in context.ReservationLogs
                            .Include(x => x.User)
                            .Include(x => x.Doctor)
                            .Include(x => x.Patient)
                    where (!model.UserId.HasValue || l.UserId == model.UserId.Value)
                          && (!model.DoctorId.HasValue || l.DoctorId == model.DoctorId.Value)
                          && (!model.PatientId.HasValue || l.PatientId == model.PatientId.Value)
                          && (!model.FromTime.HasValue || DbFunctions.CreateDateTime(l.Date.Year, l.Date.Month, l.Date.Day, l.FromTime.Hours, l.FromTime.Minutes, l.FromTime.Seconds) >= model.FromTime.Value)
                          && (!model.ToTime.HasValue || DbFunctions.CreateDateTime(l.Date.Year, l.Date.Month, l.Date.Day, l.ToTime.Hours, l.ToTime.Minutes, l.ToTime.Seconds) <= model.ToTime.Value)
                    select l;
                model.Logs = query.ToList();
            }
            return View(model);
        }


        [HttpGet]
        public ActionResult GetPatient(int id)
        {
            using (var context = new DataContext(lazyLoading: false))
            {
                var patient = context.Patients.Where(p => p.Id == id).Select(p => new { p.Id, p.FirstName, p.LastName }).SingleOrDefault();
                return JsonNet(patient);
            }
        }

        private void FillViewBag()
        {
            using (var context = new DataContext(lazyLoading: false))
            {
                ViewBag.Users = context.Users.AsNoTracking().OrderBy(x => x.FirstName).ThenBy(x => x.LastName).AsEnumerable().Select(u => new User
                {
                    Id = u.Id,
                    FirstName = u.FirstName,
                    LastName = u.LastName
                }).ToList();
                ViewBag.Doctors = context.Doctors.AsNoTracking().OrderBy(x => x.FirstName).ThenBy(x => x.LastName).AsEnumerable().Select(d => new Doctor
                {
                    Id = d.Id,
                    FirstName = d.FirstName,
                    LastName = d.LastName
                }).ToList();
            }
        }
    }
}