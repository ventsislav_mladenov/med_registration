﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using MedRegistration.Controllers;
using MedRegistration.Data;
using MedRegistration.Infrastructure;
using MedRegistration.Infrastructure.Authorization;
using MedRegistration.Infrastructure.Hubs;
using Microsoft.AspNet.SignalR;

namespace MedRegistration.Areas.Reservation.Controllers
{
    [RegistratureCheck]
    public class ReservationController : BaseController
    {
        private static readonly object registerLocker = new object();
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Reserve()
        {
            return PartialView();
        }

        [HttpGet]
        public ActionResult GetDoctorsSchedule(DateTime fromDate, DateTime toDate)
        {
            using (var context = new DataContext(lazyLoading: false))
            {
                var query = from sc in context.Schedules
                                .Include(x => x.Doctor.Title)
                                .Include(x => x.ScheduleDates)
                            where fromDate <= sc.Date && sc.Date <= toDate && sc.ScheduleDates.Any()
                            group sc by sc.Date into gr
                            let minHour = gr.SelectMany(x => x.ScheduleDates).Select(x => x.FromTime).Min()
                            let maxHour = gr.SelectMany(x => x.ScheduleDates).Select(x => x.ToTime).Max()
                            select new
                            {
                                Date = gr.Key,
                                DayMinHour = (int)Math.Floor((minHour.Hours * 60 + minHour.Minutes) / (double)60) * 60,
                                DayMaxHour = (int)Math.Ceiling((maxHour.Hours * 60 + maxHour.Minutes) / (double)60) * 60,
                                Doctors = from g in gr
                                          select new
                                          {
                                              g.DoctorId,
                                              g.Doctor.FirstName,
                                              g.Doctor.LastName,
                                              g.Note,
                                              ExamTime = g.Doctor.DefaultExamTime,
                                              Title = g.Doctor.Title.Abr,
                                              Schedule = from sc in g.ScheduleDates
                                                         select new
                                                         {
                                                             sc.Id,
                                                             sc.IsNZOK,
                                                             FromTime = sc.FromTime.Hours * 60 + sc.FromTime.Minutes,
                                                             ToTime = sc.ToTime.Hours * 60 + sc.ToTime.Minutes
                                                         }
                                          }
                            };

                var data = query.ToList();
                return JsonNet(data);
            }
        }

        [HttpGet]
        public ActionResult GetDoctorAvailableDays(DateTime currentDate)
        {
            using (var context = new DataContext(lazyLoading: false))
            {
                var toDate = currentDate.AddDays(20);
                var query = from sc in context.Schedules
                            where DateTime.Today <= sc.Date && sc.Date <= toDate && sc.ScheduleDates.Any()
                            select sc.Date;
                var data = query.ToList();
                return JsonNet(data);
            }
        }

        [HttpGet]
        public ActionResult GetDoctorScheduleForDate(DateTime date, int doctorId)
        {
            using (var context = new DataContext(lazyLoading: false))
            {
                var scheduleQuery = from d in context.ScheduleDates
                                    where d.Schedule.Date == date && d.Schedule.DoctorId == doctorId
                                    select new
                                    {
                                        d.IsNZOK,
                                        FromTime = d.FromTime.Hours * 60 + d.FromTime.Minutes,
                                        ToTime = d.ToTime.Hours * 60 + d.ToTime.Minutes
                                    };
                var reservationsQuery = from r in context.Reservations
                                        where r.DoctorId == doctorId && r.Date == date
                                        select new
                                        {
                                            FromTime = r.FromTime.Hours * 60 + r.FromTime.Minutes,
                                            ToTime = r.ToTime.Hours * 60 + r.ToTime.Minutes
                                        };
                return JsonNet(new
                {
                    schedules = scheduleQuery.ToList(),
                    reservations = reservationsQuery.ToList()
                });
            }
        }

        [HttpGet]
        public ActionResult GetReservations(DateTime fromDate, DateTime toDate, int? doctorId, TimeSpan? fromTime, TimeSpan? toTime)
        {
            using (var context = new DataContext(lazyLoading: false))
            {
                var query = from r in context.Reservations
                            where fromDate <= r.Date && r.Date <= toDate
                               && (!doctorId.HasValue || r.DoctorId == doctorId.Value)
                               && (!fromTime.HasValue || r.FromTime == fromTime.Value)
                               && (!toTime.HasValue || r.ToTime == toTime.Value)
                            group r by new { r.Date, r.DoctorId } into gr
                            select new
                            {
                                Date = gr.Key.Date,
                                DoctorId = gr.Key.DoctorId,
                                Hours = from h in gr
                                        let patientPhone = h.Patient.PatientPhones.FirstOrDefault(p => p.IsPrimary)
                                        select new
                                        {
                                            Id = h.Id,
                                            FromTime = h.FromTime,
                                            ToTime = h.ToTime,
                                            PatientId = h.Patient.Id,
                                            PatientFirstName = h.Patient.FirstName,
                                            PatientLastName = h.Patient.LastName,
                                            Note = h.Note,
                                            PatientPhone = patientPhone.Number,
                                            PaymentType = h.PaymentType.Type,
                                            CreatedBy = h.CreatedByUser.FirstName + " " + h.CreatedByUser.LastName
                                        }
                            };
                var data = query.ToList();
                return JsonNet(data);
            }
        }

        private void CreateReservation(Data.Reservation reservation, DataContext context)
        {
            reservation.CreatedBy = HttpContext.User.UserId();
            context.Reservations.Add(reservation);
            if (reservation.PaymentTypeId == 3 && reservation.PaymentInfo != null && reservation.PaymentInfo.FundId.HasValue && reservation.PaymentInfo.FundCardExpiration.HasValue)
            {
                var patient = reservation.Patient ?? context.Patients.Single(p => p.Id == reservation.PatientId);
                if (patient.PatientFundInfo == null)
                    patient.PatientFundInfo = new PatientFundInfo();
                patient.PatientFundInfo.FundId = reservation.PaymentInfo.FundId.Value;
                patient.PatientFundInfo.FundCardNumber = reservation.PaymentInfo.FundCardNumber;
                patient.PatientFundInfo.FundCardExpiration = reservation.PaymentInfo.FundCardExpiration.Value;
            }
            context.SaveChanges();
            BroadCastReservationMade(reservation, context);
            LogRegistrationOperation(reservation, OperationType.Create);
        }

        private void DeleteReservation(Data.Reservation reservation, DataContext context)
        {
            context.Reservations.Remove(reservation);
            context.SaveChanges();
            BroadCastReservationRemoved(reservation.DoctorId, reservation.Date, reservation.FromTime, reservation.ToTime);
            LogRegistrationOperation(reservation, OperationType.Delete);
        }

        private bool CheckForReservation(Data.Reservation reservation, DataContext context)
        {
            var res = context.Reservations.SingleOrDefault(rl => rl.DoctorId == reservation.DoctorId 
                && rl.Date == reservation.Date.Date 
                && rl.FromTime == reservation.FromTime 
                && rl.ToTime == reservation.ToTime);
            return res == null;
        }

        [HttpPost]
        [AntiForgeryValidate]
        public ActionResult Save(Data.Reservation reservation, Patient patient)
        {
            lock (registerLocker)
            {

                try
                {
                    using (var context = new DataContext())
                    {
                        if (!CheckForReservation(reservation, context))
                            return CreateResult(ResultType.Exists);

                        if (reservation.Id == 0)
                        {
                            if (patient != null)
                            {
                                context.Patients.Add(patient);
                                reservation.Patient = patient;
                                reservation.Note = patient.Note;
                            }
                        }
                        else
                        {
                            var oldReservation = context.Reservations.SingleOrDefault(r => r.Id == reservation.Id);
                            if (oldReservation != null)
                            {
                                DeleteReservation(oldReservation, context);
                            }
                            else
                            {
                                return CreateResult(ResultType.Deleted);
                            }
                        }
                        CreateReservation(reservation, context);
                    }
                    return CreateResult(ResultType.Success);
                }
                catch (Exception ex)
                {
                    return CreateResult(ResultType.Fail);
                }
            }
        }

        [HttpPost]
        public ActionResult RemoveReservation(int id)
        {
            lock (registerLocker)
            {
                try
                {
                    using (var context = new DataContext())
                    {
                        var reservation = context.Reservations.SingleOrDefault(x => x.Id == id);
                        if (reservation == null)
                            return CreateResult(ResultType.Deleted);
                        DeleteReservation(reservation, context);
                        return CreateResult(ResultType.Success);
                    }
                }
                catch
                {
                    return CreateResult(ResultType.Fail);
                }
            }
        }


        private void BroadCastReservationMade(Data.Reservation reservation, DataContext dataContext)
        {
            dataContext.Entry(reservation).Reference(x => x.Patient).Load();
            dataContext.Entry(reservation.Patient).Collection(x => x.PatientPhones).Load();
            dataContext.Entry(reservation).Reference(x => x.CreatedByUser).Load();
            dataContext.Entry(reservation).Reference(x => x.PaymentType).Load();

            IHubContext hubContext = GlobalHost.ConnectionManager.GetHubContext<ReservationHub>();
            //int doctorId, DateTime date, TimeSpan fromTime, TimeSpan toTime,
            //int reservationId, int patientId, string firstName, string lastName, string note, string phone
            hubContext.Clients.All.SendReservationMade(reservation.DoctorId, reservation.Date.ToUniversalDate(),
                reservation.FromTime, reservation.ToTime,
                reservation.Id, reservation.Patient.Id, reservation.Patient.FirstName, reservation.Patient.LastName,
                reservation.Note, reservation.Patient.PatientPhones.Single(p => p.IsPrimary).Number,
                reservation.PaymentType.Type,
                reservation.CreatedByUser.FullName);
        }

        private void BroadCastReservationRemoved(int doctorId, DateTime date, TimeSpan fromTime, TimeSpan toTime)
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<ReservationHub>();
            context.Clients.All.SendReservationRemoved(doctorId, date.ToUniversalDate(), fromTime, toTime);
        }

        private void LogRegistrationOperation(Data.Reservation reservation, OperationType operationType)
        {
            using (var context = new DataContext())
            {
                var log = new ReservationLog
                {
                    UserId = reservation.CreatedBy,
                    OperationType = operationType,
                    PatientId = reservation.PatientId,
                    DoctorId = reservation.DoctorId,
                    Date = reservation.Date,
                    FromTime = reservation.FromTime,
                    ToTime = reservation.ToTime
                };
                context.ReservationLogs.Add(log);
                context.SaveChanges();
            }
        }

        [HttpGet]
        public ActionResult GetReservation(int id)
        {
            using (var context = new DataContext(lazyLoading: false))
            {
                var reservation = context.Reservations
                                         .Include(r => r.Patient.PatientPhones)
                                         .Include(r => r.PaymentInfo)
                                         .SingleOrDefault(r => r.Id == id);
                if (reservation != null)
                {
                    return JsonNet(new
                            {
                                reservation.Id,
                                Patient = new
                                {
                                    reservation.Patient.Id,
                                    reservation.Patient.FirstName,
                                    reservation.Patient.LastName,
                                    reservation.Patient.IdentNumber,
                                    PhoneNumber = reservation.Patient.PatientPhones.First(p => p.IsPrimary).Number
                                },
                                reservation.Note,
                                reservation.PaymentTypeId,
                                reservation.PaymentInfo
                            });  
                }
                return CreateResult(ResultType.Deleted);
            }
        }
    }
}