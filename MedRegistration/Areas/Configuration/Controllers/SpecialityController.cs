﻿using System;
using System.Linq;
using MedRegistration.Controllers;
using System.Web.Mvc;
using MedRegistration.Data;
using MedRegistration.Infrastructure.Authorization;

namespace MedRegistration.Areas.Configuration.Controllers
{
    [AdminCheck]
    public class SpecialityController : BaseController
    {
        private static readonly object lockerDeleteSpeciality = new object();
        private static readonly object lockerDeleteSpecialityDoctor = new object();
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Save(Speciality speciality)
        {
            try
            {
                using (var context = new DataContext())
                {
                    if (speciality.Id == 0)
                    {
                        context.Specialities.Add(speciality);
                    }
                    else
                    {
                        var dbSpeciality = context.Specialities.SingleOrDefault(f => f.Id == speciality.Id);
                        if (dbSpeciality == null)
                            return CreateResult(ResultType.Deleted);
                        dbSpeciality.Description = speciality.Description;
                    }
                    context.SaveChanges();
                    return CreateResult(ResultType.Success);
                }
            }
            catch
            {
                return CreateResult(ResultType.Fail);
            }
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            lock (lockerDeleteSpeciality)
            {
                try
                {
                    using (var context = new DataContext())
                    {

                        var dbSpeciality = context.Specialities.SingleOrDefault(s => s.Id == id);
                        if (dbSpeciality == null)
                            return CreateResult(ResultType.Deleted);

                        if (dbSpeciality.Doctors.Any())
                            return CreateResult(ResultType.Fail, "Тази специалност се използва и неможе да бъде изтрита!");
                        context.Specialities.Remove(dbSpeciality);
                        context.SaveChanges();
                        return CreateResult(ResultType.Success);
                    }
                }
                catch
                {
                    return CreateResult(ResultType.Fail);
                }
            }
        }

        [HttpGet]
        public ActionResult GetDoctorsForSpeciality(int id)
        {
            using (var context = new DataContext(lazyLoading: false))
            {
                var doctors = from d in context.Doctors
                    where d.Specialities.Any(s => s.Id == id)
                    select new
                    {
                        d.Id,
                        Title = d.Title.Abr,
                        d.FirstName,
                        d.LastName
                    };
                return JsonNet(doctors.ToList());
            }
        }

        [HttpPost]
        public ActionResult DeleteDoctor(int specialityId, int doctorId)
        {
            lock (lockerDeleteSpecialityDoctor)
            {
                try
                {
                    using (var context = new DataContext())
                    {

                        var speciality = context.Specialities.SingleOrDefault(s => s.Id == specialityId);
                        if (speciality == null)
                            return CreateResult(ResultType.Deleted);

                        var doctor = context.Doctors.SingleOrDefault(d => d.Id == doctorId);
                        if (doctor == null)
                            return CreateResult(ResultType.Deleted);

                        speciality.Doctors.Remove(doctor);
                        
                        context.SaveChanges();
                        return CreateResult(ResultType.Success);
                    }
                }
                catch
                {
                    return CreateResult(ResultType.Fail);
                }
            }
        }

        [HttpGet]
        public ActionResult SelectDoctor()
        {
            return PartialView();
        }

        [HttpGet]
        public ActionResult GetDoctorsNotInSpeciality(int id)
        {
            using (var context = new DataContext(lazyLoading: false))
            {
                var doctors = from d in context.Doctors
                              where d.Specialities.All(s => s.Id != id)
                              select new
                              {
                                  d.Id,
                                  Title = d.Title.Abr,
                                  d.FirstName,
                                  d.LastName
                              };
                return JsonNet(doctors.ToList());
            }
        }

        [HttpPost]
        public ActionResult AddDoctorToSpeciality(int specialityId, int[] doctors)
        {
            lock (lockerDeleteSpecialityDoctor)
            {
                try
                {
                    using (var context = new DataContext())
                    {

                        var speciality = context.Specialities.SingleOrDefault(s => s.Id == specialityId);
                        if (speciality == null)
                            return CreateResult(ResultType.Deleted);

                        foreach (var doctorId in doctors)
                        {
                            var doctor = context.Doctors.SingleOrDefault(d => d.Id == doctorId);
                            if (doctor == null)
                                continue;

                            speciality.Doctors.Add(doctor);
                        }

                        context.SaveChanges();
                        return CreateResult(ResultType.Success);
                    }
                }
                catch
                {
                    return CreateResult(ResultType.Fail);
                }
            }
        }

    }
}