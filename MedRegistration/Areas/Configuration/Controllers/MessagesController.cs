﻿using System;
using System.Linq;
using System.Web.Mvc;
using MedRegistration.Controllers;
using MedRegistration.Data;
using MedRegistration.Infrastructure.Authorization;

namespace MedRegistration.Areas.Configuration.Controllers
{
    [MessageCheck]
    public class MessagesController : BaseController
    {
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Send(Message message)
        {
            try
            {
                using (var context = new DataContext())
                {
                    message.UserId = HttpContext.User.UserId();
                    context.Messages.Add(message);
                    context.SaveChanges();
                    return CreateResult(ResultType.Success);
                }
            }
            catch (Exception ex)
            {
                return CreateResult(ResultType.Fail, ex.Message);
            }
        }

        [HttpGet]
        public ActionResult GetMessages()
        {
            try
            {
                using (var context = new DataContext(lazyLoading: false))
                {
                    var userId = HttpContext.User.UserId();
                    var messages = from m in context.Messages
                                   where m.UserId == userId
                                   orderby m.SendAt descending
                                   select new
                                   {
                                       m.Text,
                                       m.SendAt,
                                       Type = (int)m.Type,
                                       Validity = (int)m.Validity,
                                       m.ValidFrom,
                                       m.ValidTo,
                                       Recipients = from r in m.Recipients
                                                    select new
                                                    {
                                                        r.Recipient.Id,
                                                        r.Recipient.FirstName,
                                                        r.Recipient.LastName,
                                                        r.ReadAt
                                                    }
                                   };
                    return JsonNet(messages.ToList());
                }
            }
            catch (Exception ex)
            {
                return CreateResult(ResultType.Fail, ex.Message);
            }
        }
    }
}