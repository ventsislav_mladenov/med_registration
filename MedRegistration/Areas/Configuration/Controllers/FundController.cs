﻿using System;
using System.Linq;
using System.Web.Mvc;
using MedRegistration.Controllers;
using MedRegistration.Data;
using MedRegistration.Infrastructure.Authorization;

namespace MedRegistration.Areas.Configuration.Controllers
{
    [AdminCheck]
    public class FundController : BaseController
    {
        private static readonly object locker = new object();
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Save(Fund fund)
        {
            try
            {
                using (var context = new DataContext())
                {
                    if (fund.Id == 0)
                    {
                        context.Funds.Add(fund);
                    }
                    else
                    {
                        var dbFund = context.Funds.SingleOrDefault(f => f.Id == fund.Id);
                        if (dbFund == null)
                            return CreateResult(ResultType.Deleted);
                        dbFund.Name = fund.Name;
                    }
                    context.SaveChanges();
                    return CreateResult(ResultType.Success);
                }
            }
            catch (Exception)
            {
                return CreateResult(ResultType.Fail);
            }
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            lock (locker)
            {
                try
                {
                    using (var context = new DataContext())
                    {

                        var dbFund = context.Funds.SingleOrDefault(f => f.Id == id);
                        if (dbFund == null)
                            return CreateResult(ResultType.Deleted);

                        if (dbFund.PatientFundInfoes.Any() || dbFund.PaymentInfoes.Any())
                            return CreateResult(ResultType.Fail, "Този фонд се използва и неможе да бъде изтрит!");
                        context.Funds.Remove(dbFund);
                        context.SaveChanges();
                        return CreateResult(ResultType.Success);
                    }
                }
                catch
                {
                    return CreateResult(ResultType.Fail);
                }
            }
        }
    }
}