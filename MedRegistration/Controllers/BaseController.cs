﻿using System;
using System.Linq;
using System.Web.Mvc;
using MedRegistration.Data;
using MedRegistration.Infrastructure;
using MedRegistration.Infrastructure.Authorization;

namespace MedRegistration.Controllers
{
    [Authorize]
    public class BaseController : Controller
    {
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            filterContext.Controller.ViewBag.AssemblyVersion = System.Reflection.Assembly.GetAssembly(typeof(MvcApplication)).GetName().Version.ToString();
            filterContext.Controller.ViewBag.AdminViewClass = filterContext.HttpContext.User.IsAdmin() ? "" : "hidden";
            filterContext.Controller.ViewBag.ReportViewClass = filterContext.HttpContext.User.IsAdmin() || filterContext.HttpContext.User.IsReport() ? "" : "hidden";
            filterContext.Controller.ViewBag.MessageViewClass = filterContext.HttpContext.User.IsAdmin() || filterContext.HttpContext.User.IsMessage() ? "" : "hidden";
            var lang = Translation.DefaultLanguage;
            var langCookie = filterContext.HttpContext.Request.Cookies["lang"];
            if (langCookie != null)
            {
                lang = langCookie.Value;
                if (!Translation.Languages.ContainsKey(lang))
                    lang = Translation.DefaultLanguage;
            }
            filterContext.Controller.ViewBag.Lang = lang;
        }

        public JsonResult JsonNet<T>(T data)
        {
            return new JsonNetResult<T>(data);
        }

        public ActionResult GetFunds()
        {
            using (var context = new DataContext(lazyLoading: false))
            {
                return JsonNet(context.Funds.Where(f => f.Id > 0).Select(f => new { f.Id, f.Name }).ToList());
            }
        }

        public ActionResult GetTitles()
        {
            using (var context = new DataContext(lazyLoading: false))
            {
                return JsonNet(context.Titles.Select(f => new { f.Id, f.Abr, f.Description }).ToList());
            }
        }

        public ActionResult GetSpecialities()
        {
            using (var context = new DataContext(lazyLoading: false))
            {
                return JsonNet(context.Specialities.Select(s => new { s.Id, s.Description }).ToList());
            }
        }

        public virtual ActionResult SearchPatients(string search)
        {
            using (var context = new DataContext(lazyLoading: false))
            {
                var query = context.Patients.AsQueryable();
                var searches = search.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var s in searches)
                {
                    string s1 = s;
                    query = query.Where(p => p.FirstName.Contains(s1) || p.LastName.Contains(s1) || p.IdentNumber.Contains(s1) || p.Email.Contains(s1)
                                             || p.PatientPhones.Any(ph => ph.Number.Contains(s1)));
                }
                var result = from p in query
                             let phone = p.PatientPhones.FirstOrDefault(pp => pp.IsPrimary)
                             orderby p.FirstName, p.LastName
                             select new
                                        {
                                            p.Id,
                                            p.FirstName,
                                            p.LastName,
                                            p.IdentNumber,
                                            PhoneNumber = phone.Number
                                        };
                return JsonNet(result.ToList().Take(50));
            }
        }

        protected enum ResultType
        {
            Success = 1,
            Fail = 2,
            Deleted = 3,
            Exists = 4
        }

        protected ActionResult CreateResult(ResultType type, string message = "")
        {
            return JsonNet(new
            {
                result = (int)type,
                msg = message
            });
        }
    }
}