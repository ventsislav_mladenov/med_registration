﻿using System;
using System.Linq;
using System.Web.Mvc;
using MedRegistration.Data;
using MedRegistration.Infrastructure.Authorization;

namespace MedRegistration.Controllers
{
    public class HomeController : BaseController
    {
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult GetMessages()
        {
            try
            {
                using (var context = new DataContext(lazyLoading: false))
                {
                    var userId = HttpContext.User.UserId();
                    var messages = from m in context.Messages
                                   where m.Recipients.Any(r => r.UserId == userId)
                                   && (
                                        (m.Validity == MessageValidityType.OneTime && m.Recipients.Any(r => r.UserId == userId && !r.ReadAt.HasValue)) ||
                                        (m.Validity == MessageValidityType.Interval && (!m.ValidFrom.HasValue || m.ValidFrom.Value <= DateTime.Today)
                                                                                    && (!m.ValidTo.HasValue || m.ValidTo.Value >= DateTime.Today)
                                        )
                                      )
                                   orderby m.SendAt descending
                                   select new
                                   {
                                       m.Id,
                                       m.Text,
                                       m.SendAt,
                                       Type = (int)m.Type,
                                       Validity = (int)m.Validity,
                                       Sender = m.Sender.FirstName + " " + m.Sender.LastName
                                   };
                    return JsonNet(messages.ToList());
                }
            }
            catch (Exception ex)
            {
                return CreateResult(ResultType.Fail, ex.Message);
            }
        }

        [HttpPost]
        public ActionResult ReadMessage(int id)
        {
            try
            {
                using (var context = new DataContext())
                {
                    var userId = HttpContext.User.UserId();
                    var recipient = context.MessageRecipients.SingleOrDefault(m => m.MessageId == id && m.UserId == userId);
                    if (recipient == null)
                        return CreateResult(ResultType.Deleted);
                    recipient.ReadAt = DateTime.Now;
                    context.SaveChanges();
                    return CreateResult(ResultType.Success);
                }
            }
            catch (Exception ex)
            {
                return CreateResult(ResultType.Fail, ex.Message);
            }
        }
    }
}