﻿app.controller("doctorsController", ["$scope", '$http', "$modal", "$rootScope", function ($scope, $http, $modal, $rootScope) {
    var _self = this;
    $scope.doctors = [];

    _self.loadDoctors = function () {
        $http({
            method: 'GET',
            url: '/Common/Doctor/GetDoctors'
        }).success(function (doctors) {
            $scope.doctors = doctors;
        });
    };

    _self.loadDoctors();


    _self.openDoctor = function (id) {
        var addDoctorInstance = $modal.open({
            templateUrl: '/Common/Doctor/Add',
            controller: 'doctorAddController',
            resolve: {
                id: function () {
                    return id;
                }
            }
        });
        addDoctorInstance.result.then(function () {
            _self.loadDoctors();
        });
    };

    $scope.edit = function (id) {
        _self.openDoctor(id);
    };

    $scope.add = function () {
        _self.openDoctor(0);
    };
}]);

app.controller('doctorAddController', [
    '$scope', '$http', '$q', '$modalInstance', 'id', function ($scope, $http, $q, $modalInstance, id) {
        var __self = this;
        $scope.data = {
            titles: [],
            specialities: [
                { id: 0, description: $scope.translation.texts.selectSpecialty }
            ]
        };


        $scope.model = {
            id: id,
            title: {},
            firstName: '',
            lastName: '',
            phoneNumber: '',
            defaultExamTime: 20,
            specialities: [],
            errors: []
        };

        __self.loadTitles = function () {
            return $http({
                method: 'GET',
                url: '/Common/Doctor/GetTitles'
            }).success(function (titles) {
                $scope.data.titles.pushAll(titles);
                $scope.model.title = titles[0];
            });
        };

        __self.loadSpecialities = function () {
            return $http({
                method: 'GET',
                url: '/Common/Doctor/GetSpecialities'
            }).success(function (specialities) {
                $scope.data.specialities.pushAll(specialities);
            });
        };

        __self.getCanBeDeleted = function () {
            return $http({
                method: 'GET',
                url: '/Common/Doctor/CanBeDeleted',
                params: {
                    id: id
                }
            }).success(function (res) {
                $scope.model.canBeDeleted = res;
            });
        };

        __self.loadData = function () {
            var waits = [];
            waits.push(__self.loadTitles());
            waits.push(__self.loadSpecialities());
            waits.push(__self.getCanBeDeleted());
            return $q.all(waits);
        };

        __self.loadDoctor = function () {
            $http({
                method: 'GET',
                url: '/Common/Doctor/GetDoctor',
                params: {
                    id: id
                }
            }).success(function (doctor) {
                $scope.model.title = $scope.data.titles.find(function (t) { return t.id === doctor.titleId; });
                $scope.model.firstName = doctor.firstName;
                $scope.model.lastName = doctor.lastName;
                $scope.model.phoneNumber = doctor.phoneNumber;
                $scope.model.defaultExamTime = doctor.defaultExamTime;

                for (var i = 0; i < doctor.specialities.length; i++) {
                    var specialty = doctor.specialities[i];
                    $scope.model.specialities.push({
                        type: $scope.data.specialities.find(function(x) { return x.id == specialty.id; }),
                        types: _.filter($scope.data.specialities, function (s) {
                            if (s.id === 0)
                                return true;
                            return !_.any($scope.model.specialities, function (ms) { return ms.type.id === s.id; });
                        })
                    });
                }
            });
        };

        __self.loadData().then(function () {
            if (id > 0) {
                __self.loadDoctor();
            }
        });

        $scope.save = function () {
            $scope.model.errors.clear();
            if (!$scope.model.firstName) {
                $scope.model.errors.push($scope.translation.texts.pleaseEnterFirstName);
            }
            if (!$scope.model.lastName) {
                $scope.model.errors.push($scope.translation.texts.pleaseEnterLastName);
            }
            if ($scope.model.errors.length > 0)
                return false;

            var doctor = {
                id: $scope.model.id,
                titleId: $scope.model.title.id,
                firstName: $scope.model.firstName,
                lastName: $scope.model.lastName,
                phoneNumber: $scope.model.phoneNumber,
                defaultExamTime: $scope.model.defaultExamTime
            };
            var specialities = [];
            for (var i = 0; i < $scope.model.specialities.length; i++) {
                var speciality = $scope.model.specialities[i];
                if (speciality.type.id > 0)
                    specialities.push(speciality.type);
            }
            $http(
            {
                method: 'POST',
                url: '/Common/Doctor/Save',
                data: {
                    doctor: doctor,
                    specialities: specialities
                }
            }).success(function (res) {
                __self.ok();
            }).error(function (err) {
                $scope.model.errors.push(err);
            });

            return true;
        };

        __self.ok = function () {
            $modalInstance.close();
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

        $scope.delete = function () {
            if (confirm($scope.translation.texts.areYouSureWantDeleteDoctor + ': ' + $scope.model.firstName + ' ' + $scope.model.lastName)) {
                $http(
                {
                    method: 'POST',
                    url: '/Common/Doctor/Delete',
                    data: {
                        id: $scope.model.id
                    }
                }).success(function (res) {
                    __self.ok();
                }).error(function (err) {
                    $scope.model.errors.push(err);
                });
            }
        }

        $scope.addNewSpeciality = function () {
            if (_.any($scope.model.specialities, function (s) { return s.type.id === 0; }))
                return;
            $scope.model.specialities.push({
                type: $scope.data.specialities[0],
                types: _.filter($scope.data.specialities, function (s) {
                    if (s.id === 0)
                        return true;
                    return !_.any($scope.model.specialities, function (ms) { return ms.type.id === s.id; });
                })
            });
        };

        $scope.removeSpeciality = function (speciality) {
            $scope.model.specialities.remove(speciality);
        };
    }
]);