﻿var loginApp = angular.module("loginApp", ['ui.translate']);

loginApp.controller('loginController', [
    '$scope', 'translationService', function ($scope, translation) {
        $scope.translation = translation;
        translation.applyTranslation();
    }
]);