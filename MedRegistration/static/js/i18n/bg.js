﻿(
    function (translation) {
        translation.texts = {
            /* Common */
            "add": "Добави",
            "save": "Запис",
            "cancel": "Отказ",
            "delete": "Изтрий",
            /* Login */
            "loginTitle": "Вход в системата",
            "userName": "Потребител",
            "password": "Парола",
            "rememberMe": "Запомни ме",
            "login": "Влез",
            "useChromeMsg": "За най-добра и бърза работа използвайте",
            /* Menu */
            "home": "Начало",
            "nomenclature": "Номенклатура",
            "doctors": "Лекари",
            "patients": "Пациенти",
            "schedule": "График",
            "reports": "Справки",
            "reservationLog": "Лог на резервациите",
            "configuration": "Настройки",
            "users": "Потребители",
            "changePersonalInfo": "Промяна лични данни",
            "specialties": "Специалности",
            "funds": "Фондове",
            "logout": "Изход",
            /* Home */
            "welcome": "Добре дошли",
            "messages": "Съобщения",
            "noNewMessages": "Няма нови съобщения",
            "messageReceivedAt": 'Получено на',
            "messageFrom": 'Изпратено от',
            "messageBody": 'Текст',
            /* Doctors */
            "addNewDoctor": "Добави нов лекар",
            "title": "Титла",
            "firstName": "Име",
            "lastName": "Фамилия",
            "phone": "Телефон",
            "examTime": "Време за преглед (мин.)",
            "selectSpecialty": "-- Избери Специалност --",
            "pleaseEnterFirstName": "Моля въведете първо име!",
            "pleaseEnterLastName": "Моля въведете фамилия!",
            "areYouSureWantDeleteDoctor": "Сигурни ли сте, че искате да изтриете лекаря",
        };
    }
)(window.translation = window.translation || {});