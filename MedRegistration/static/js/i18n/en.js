﻿(
    function (translation) {
        translation.texts = {
            /* Common */
            "add": "Add",
            "save": "Save",
            "cancel": "Cancel",
            "delete": "Delete",
            /* Login */
            "loginTitle": "Login",
            "userName": "User Name",
            "password": "Password",
            "rememberMe": "Remember me",
            "login": "Login",
            "useChromeMsg": "For best performance and user experience use",
            /* Menu */
            "home": "Home",
            "nomenclature": "Nomenclature",
            "doctors": "Doctors",
            "patients": "Patients",
            "schedule": "Schedule",
            "reports": "Reports",
            "reservationLog": "Reservation log",
            "configuration": "Configuration",
            "users": "Users",
            "changePersonalInfo": "Change personal info",
            "specialties": "Specialties",
            "funds": "Funds",
            "logout": "Log out",
            /* Home */
            "welcome": "Welcome",
            "messages": "Messages",
            "noNewMessages": "No new messages",
            "messageReceivedAt": 'Received At',
            "messageFrom": 'From',
            "messageBody": 'Body',
            /* Doctors */
            "addNewDoctor": "Add new doctor",
            "title": "Title",
            "firstName": "First name",
            "lastName": "Last name",
            "phone": "Phone",
            "examTime": "Exam time (min)",
            "selectSpecialty": "-- Select specialty --",
            "pleaseEnterFirstName": "Please enter first name!",
            "pleaseEnterLastName": "Please enter last name!",
            "areYouSureWantDeleteDoctor": "Are you sure you want to delete the doctor",
    };
    }
)(window.translation = window.translation || {});
