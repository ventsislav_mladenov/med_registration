﻿app.controller('registrationAddController', ['$scope', '$http', '$modalInstance', '$q', 'timeConverter', 'info', 'dateHelper',
    function ($scope, $http, $modalInstance, $q, timeConverter, info, dateHelper) {
        var __self = this;
        $scope.data = {
            funds: [
                { id: 0, name: '-- Изберете Фонд --' }
            ],
            availableDays: [],
            hours: [],
            errors: [],
            searchText: '',
            patients: []
        };

        __self.initModel = function () {
            return {
                id: 0,
                date: info.date,
                doctor: info.doctor,
                nzok: info.hour.isnzok,
                selectedPatient: 0,
                note: '',
                isNewPatient: 0,
                newPatient: {
                    firstName: '',
                    lastName: '',
                    phoneNumber: '',
                    note: ''
                },
                paymentType: 1,
                fund: $scope.data.funds[0],
                fundCardNumber: '',
                fundCardExpiration: null,
                time: {
                    from: info.hour.from
                }
            };
        };

        $scope.model = __self.initModel();

        __self.loadReservation = function (id) {
            $http(
            {
                url: '/Reservation/Reservation/GetReservation',
                params: {
                    id: id,
                }
            }).success(function (res) {
                $scope.model = __self.initModel();
                $scope.model.id = res.id;
                $scope.data.patients.clear();
                $scope.data.patients.push(res.patient);
                $scope.model.selectedPatient = res.patient.id;
                $scope.model.paymentType = res.paymentTypeId;
                $scope.model.note = res.note;
                if (res.paymentTypeId === 3 && res.paymentInfo) {
                    $scope.model.fund = $scope.data.funds.find(function (f) { return f.id == res.paymentInfo.fundId; });
                    $scope.model.fundCardNumber = res.paymentInfo.fundCardNumber;
                    $scope.model.fundCardExpiration = dateHelper.parseDate(res.paymentInfo.fundCardExpiration);
                }
            });
        };

        __self.searchPatient = function (query) {
            $http(
            {
                url: '/Reservation/Reservation/SearchPatients',
                params: {
                    search: query,
                    selectedId: $scope.model.selectedPatient,
                }
            }).success(function (res) {
                $scope.data.patients.clear();
                $scope.data.patients.pushAll(res);
            });
        };

        __self.buildHours = function (schedules, reservations) {
            var examTime = info.doctor.examTime;
            var sortedSchedules = _.sortBy(schedules, function (x) { return x.fromTime; });
            var hours = [];

            for (var i = 0; i < sortedSchedules.length; i++) {
                var schedule = sortedSchedules[i];
                if (schedule.toTime === 1439)
                    schedule.toTime = 1440;

                var time = schedule.fromTime + examTime;
                while (time < schedule.toTime) {
                    if (!_.any(reservations, function (r) { return r.fromTime == time - examTime && r.toTime == time; })) {
                        hours.push({
                            work: 1,
                            from: time - examTime,
                            to: time,
                            isnzok: schedule.isnzok,
                        });
                    }
                    time += examTime;
                }
                if (time - examTime < schedule.toTime) {
                    if (!_.any(reservations, function (r) { return r.fromTime == time - examTime && r.toTime == schedule.toTime; })) {
                        hours.push({
                            work: 1,
                            from: time - examTime,
                            to: schedule.toTime,
                            isnzok: schedule.isnzok
                        });
                    }
                }
            }
            for (var j = 0; j < hours.length; j++) {
                var h = hours[j];
                h.text = timeConverter.convertToHours(h.from) + ' - ' + timeConverter.convertToHours(h.to);
            }
            return hours;
        };

        __self.loadHours = function (date) {
            if (!date)
                return;
            $http({
                method: 'GET',
                url: '/Reservation/Reservation/GetDoctorScheduleForDate',
                params: {
                    date: dateHelper.dateToString(date),
                    doctorId: info.doctor.doctorId
                }
            }).success(function (data) {
                $scope.data.hours.clear();
                if (info.hour.reservation) {
                    var reservation = data.reservations.find(function (r) { return r.fromTime == info.hour.from; });
                    data.reservations.remove(reservation);
                }
                $scope.data.hours.pushAll(__self.buildHours(data.schedules, data.reservations));

                var hour = $scope.data.hours.find(function (h) { return h.from === $scope.model.time.from; });
                if (hour) {
                    $scope.model.time = hour;
                } else {
                    $scope.model.time = $scope.model.hours[0];
                }
            });
        };

        __self.loadAvailableDays = function () {
            return $http({
                method: 'GET',
                url: '/Reservation/Reservation/GetDoctorAvailableDays',
                params: {
                    currentDate: dateHelper.dateToString(info.date)
                }
            }).success(function (days) {
                $scope.data.availableDays.pushAll(_.map(days, function (d) {
                    return dateHelper.parseDate(d);
                }));
            });
        };

        __self.loadFunds = function () {
            return $http({
                method: 'GET',
                url: '/Reservation/Reservation/GetFunds'
            }).success(function (funds) {
                $scope.data.funds.pushAll(funds);
            });
        };

        __self.loadData = function () {
            var waits = [];
            waits.push(__self.loadAvailableDays());
            waits.push(__self.loadFunds());
            return $q.all(waits);
        };

        $scope.$watch('model.date', function (newVal) {
            if (!newVal)
                return;
            __self.loadHours(newVal);
        });

        $scope.$watch('data.searchText', function (newVal) {
            if (!newVal || newVal.length <= 2)
                return;
            __self.searchPatient(newVal);
        });

        $scope.onSelectPatient = function (patient) {
            $scope.model.selectedPatient = patient.id;
            $http({
                method: 'GET',
                url: '/Common/Patient/GetPatientFundInfo',
                params: {
                    id: patient.id
                }
            }).success(function (res) {
                if (res) {
                    $scope.model.paymentType = 3;
                    $scope.model.fund = $scope.data.funds.find(function (f) {
                        return f.id === res.fundId;
                    });
                    $scope.model.fundCardNumber = res.fundCardNumber;
                    $scope.model.fundCardExpiration = dateHelper.parseDate(res.fundCardExpiration);
                } else {
                    $scope.model.paymentType = 1;
                    $scope.model.fund = $scope.data.funds[0];
                    $scope.model.fundCardNumber = '';
                    $scope.model.fundCardExpiration = null;
                }
            });
        };

        __self.closeModal = function () {
            $modalInstance.close();
        };

        __self.validateModel = function () {
            $scope.data.errors = [];
            if ($scope.model.isNewPatient === 0 && !$scope.model.selectedPatient) {
                $scope.data.errors.push('Моля изберете пациент!');
            }
            if ($scope.model.paymentType === 3 && $scope.model.fund.id > 0 && (!$scope.model.fundCardNumber || !$scope.model.fundCardExpiration)) {
                $scope.data.errors.push("Моля въведете номер и/или валидност на фондовата карта!");
            }
            if ($scope.model.isNewPatient === 1) {
                if (!$scope.model.newPatient.firstName) {
                    $scope.data.errors.push('Моля попълнете първо име!');
                }
                if (!$scope.model.newPatient.lastName) {
                    $scope.data.errors.push('Моля попълнете второ име!');
                }
                if (!$scope.model.newPatient.phoneNumber) {
                    $scope.data.errors.push('Моля попълнете телефон!');
                }
            }
            return $scope.data.errors.length === 0;
        }

        $scope.select = function () {
            if (!__self.validateModel())
                return;

            var reservation = {
                id: $scope.model.id,
                doctorId: info.doctor.doctorId,
                date: dateHelper.dateToString($scope.model.date),
                fromTime: timeConverter.convertToHours($scope.model.time.from),
                toTime: timeConverter.convertToHours($scope.model.time.to),
                paymentTypeId: $scope.model.paymentType,
                note: $scope.model.note
            };

            if (reservation.paymentTypeId === 3 && $scope.model.fund.id > 0) {
                reservation.paymentInfo = {
                    fundId: $scope.model.fund.id,
                    fundCardNumber: $scope.model.fundCardNumber,
                    fundCardExpiration: dateHelper.dateToString($scope.model.fundCardExpiration)
                };
            }

            var patient = null;

            if ($scope.model.isNewPatient === 0) {
                reservation.patientId = $scope.model.selectedPatient;
            } else {
                patient = {
                    firstName: $scope.model.newPatient.firstName,
                    lastName: $scope.model.newPatient.lastName,
                    identNumberTypeId: 1,
                    genderId: 1,
                    note: $scope.model.newPatient.note,
                    patientPhones: [
                        {
                            typeId: 3,
                            number: $scope.model.newPatient.phoneNumber,
                            isPrimary: true
                        }
                    ]
                };
            }

            $http(
            {
                method: 'POST',
                url: '/Reservation/Reservation/Save',
                data: {
                    reservation: reservation,
                    patient: patient
                }
            }).success(function (res) {
                if (res.result == 4) {
                    $scope.data.errors.push('Този час вече е резервиран!');
                }
                else if (res.result == 3) {
                    $scope.data.errors.push('Някой вече е променил този час!');
                }
                else {
                    __self.closeModal();
                }
            }).error(function (err) {
                $scope.data.errors.push(err);
            });
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

        __self.loadData().then(function() {
            if (info.hour.reservation) {
                $scope.model.id = info.hour.reservation.id;
                __self.loadReservation($scope.model.id);
            }
        });


    }]);

