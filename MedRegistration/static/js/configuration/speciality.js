﻿app.controller('specialityController', [
    '$scope', '$http', '$modal', function ($scope, $http, $modal) {
        $scope.specialities = [];
        $scope.selected = null;
        $scope.error = '';
        var __self = this;

        __self.refresh = function () {
            $scope.specialities.clear();
            $http({
                method: 'GET',
                url: '/Configuration/Speciality/GetSpecialities'
            }).success(function (specialities) {
                $scope.specialities.pushAll(_.map(specialities, function (s) {
                    s.originDescription = s.description;
                    s.readOnly = true;
                    s.selected = false;
                    s.doctors = [];
                    return s;
                }));
            });
        }

        __self.refresh();


        $scope.edit = function (speciality) {
            speciality.readOnly = false;
        };

        $scope.save = function (speciality) {
            $scope.error = '';
            if (!speciality.description) {
                $scope.error = 'Моля попълнете специалност.';
                return;
            }
            $http({
                method: 'POST',
                url: '/Configuration/Speciality/Save',
                data: {
                    id: speciality.id,
                    description: speciality.description
                }
            }).success(function () { __self.refresh(); });
        };

        $scope.add = function () {
            $scope.specialities.push({
                id: 0,
                description: '',
                readOnly: false
            });
        };

        $scope.cancel = function (speciality) {
            if (speciality.id > 0) {
                speciality.description = speciality.originDescription;
                speciality.readOnly = true;
            } else {
                $scope.specialities.remove(speciality);
            }
        };

        $scope.delete = function (id) {
            $scope.error = '';
            if (confirm('Сигурни ли сте че искате да изтриете специалноста?')) {
                $http({
                    method: 'POST',
                    url: '/Configuration/Speciality/Delete',
                    data: {
                        id: id
                    }
                }).success(function (res) {
                    if (res.msg) {
                        $scope.error = res.msg;
                    } else {
                        __self.refresh();
                    }
                });
            }
        };

        $scope.select = function (speciality) {
            if (speciality.id == 0)
                return;
            if (speciality.selected) {
                speciality.selected = false;
                $scope.selected = null;
            } else {
                for (var i = 0; i < $scope.specialities.length; i++) {
                    $scope.specialities[i].selected = false;
                }
                speciality.selected = true;
                $scope.selected = speciality;
                __self.loadDoctors();
            }
        };

        __self.loadDoctors = function() {
            $http({
                method: 'GET',
                url: '/Configuration/Speciality/GetDoctorsForSpeciality',
                params: {
                    id: $scope.selected.id
                }
            }).success(function (doctors) {
                $scope.selected.doctors.clear();
                $scope.selected.doctors.pushAll(doctors);
            });
        };

        $scope.deleteDoctor = function (doctor) {
            if (!$scope.selected)
                return;
            $http({
                method: 'POST',
                url: '/Configuration/Speciality/DeleteDoctor',
                data: {
                    specialityId: $scope.selected.id,
                    doctorId: doctor.id
                }
            }).success(function (res) {
                __self.loadDoctors();
            });
        };

        $scope.addDoctor = function() {
            if (!$scope.selected)
                return;
            var addDoctorInstance = $modal.open({
                templateUrl: '/Configuration/Speciality/SelectDoctor',
                controller: 'specialityDoctorAddController',
                resolve: {
                    id: function () {
                        return $scope.selected.id;
                    }
                }
            });
            addDoctorInstance.result.then(function (doctors) {
                $http({
                    method: 'POST',
                    url: '/Configuration/Speciality/AddDoctorToSpeciality',
                    data: {
                        specialityId: $scope.selected.id,
                        doctors: doctors
                    }
                }).success(function (res) {
                    __self.loadDoctors();
                });
            });
        };
    }
]);

app.controller('specialityDoctorAddController', [
    '$scope', '$http', '$modalInstance', 'id', function ($scope, $http, $modalInstance, id) {
        $scope.doctors = [];
        var __self = this;

        __self.loadDoctors = function () {
            $http({
                method: 'GET',
                url: '/Configuration/Speciality/GetDoctorsNotInSpeciality',
                params: {
                    id: id
                }
            }).success(function (doctors) {
                $scope.doctors.clear();
                $scope.doctors.pushAll(_.map(doctors, function(d) {
                    d.selected = false;
                    return d;
                }));
            });
        };

        __self.loadDoctors();

        $scope.ok = function () {
            var selected = [];
            for (var i = 0; i < $scope.doctors.length; i++) {
                if ($scope.doctors[i].selected)
                    selected.push($scope.doctors[i].id);
            }
            $modalInstance.close(selected);
        };

        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };
    }
]);