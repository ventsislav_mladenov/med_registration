﻿using System;
using System.Globalization;

namespace MedRegistration.Infrastructure
{
    public static class RazorHelper
    {
        public static string ToUniversalDateTime(this DateTime? dateTime)
        {
            if (dateTime.HasValue)
            {
                return dateTime.Value.ToString("yyyy-MM-ddTHH:mm", CultureInfo.InvariantCulture);
            }
            return string.Empty;
        }

        public static string ToUniversalDate(this DateTime? dateTime)
        {
            if (dateTime.HasValue)
            {
                return dateTime.Value.ToUniversalDate();
            }
            return string.Empty;
        }

        public static string ToUniversalDate(this DateTime dateTime)
        {
            return dateTime.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
        }

        public static string ToUserDate(this DateTime? date)
        {
            if (date.HasValue)
            {
                return date.Value.ToUserDate();
            }
            return string.Empty;
        }

        public static string ToUserDate(this DateTime date)
        {
            return date.ToString("dd'.'MM'.'yyyy", CultureInfo.InvariantCulture);
        }

        public static string ToUserDateTime(this DateTime dateTime)
        {
            return dateTime.ToString("dd'.'MM'.'yyyy HH:mm", CultureInfo.InvariantCulture);
        }

        public static string ToUserTime(this TimeSpan? time)
        {
            if (time.HasValue)
            {
                return time.Value.ToUserTime();
            }
            return string.Empty;
        }

        public static string ToUserTime(this TimeSpan time)
        {
            return time.ToString(@"hh\:mm");
        }
    }
}