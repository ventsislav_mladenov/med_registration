﻿using System;
using System.Globalization;
using System.Web.Mvc;

namespace MedRegistration.Infrastructure
{
    public class DateTimeBinder : IModelBinder
    {
        #region IModelBinder Members

        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var value = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);
            if (value == null)
                return null;
            var valueStr = ((string)value.ConvertTo(typeof(string))).Trim('"');
            if (string.IsNullOrWhiteSpace(valueStr))
                return null;
            //2014-07-06T21:00:00.000Z
            if (valueStr.IndexOf('.') > -1)
                return DateTime.ParseExact(valueStr, "yyyy-MM-ddTHH:mm:ss.fffZ", CultureInfo.InvariantCulture);
            else
                return DateTime.ParseExact(valueStr, "yyyy-MM-ddTHH:mm:ssZ", CultureInfo.InvariantCulture);
        }

        #endregion

    }
}