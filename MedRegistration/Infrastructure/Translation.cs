﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MedRegistration.Infrastructure
{
    public class Translation
    {
        public static readonly string DefaultLanguage = "en";
        public static readonly Dictionary<string, string> Languages = new Dictionary<string, string>
                                                                                                    {
                                                                                                        {"bg", "Български"},
                                                                                                        {"en", "English"}
                                                                                                    };
    }
}