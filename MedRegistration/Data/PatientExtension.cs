﻿namespace MedRegistration.Data
{
    public partial class Patient
    {
        public string FullName
        {
            get { return FirstName + " " + LastName; }
        }
    }
}