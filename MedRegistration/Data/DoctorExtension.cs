﻿namespace MedRegistration.Data
{
    public partial class Doctor
    {
        public string FullName
        {
            get { return FirstName + " " + LastName; }
        }
    }
}